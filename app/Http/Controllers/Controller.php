<?php

namespace App\Http\Controllers;

use App\Mail\Contact;
use App\Models\Actuality;
use App\Models\Event;
use App\Models\Payment;
use App\Models\Text;
use App\Models\Worship;
use Carbon\Carbon;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Kkiapay\Kkiapay;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function index()
    {
        $about = $this->text('about');
        $about_title = $this->text('about_title');

        $vision = $this->text('vision');
        $integrite = $this->text('integrite');
        $trinite = $this->text('trinite');
        $text_pray = $this->text('text_pray');
       // $about = $this->text('actualities');

        $events = Event::take(2)->get();
        $actualities = Actuality::take(4)->get();
        return view('index',compact('about','about_title','events','actualities'));
    }
    public function text ($param){
        $text = Text::whereName($param)->first();
        if ($text)
            return $text->content ;
        return false;

    }

    public function events()
    {
        $events = Event::all();
        return view('events',compact('events'));
    }

    public function actuality(Request $request)
    {
        $actuality = Actuality::whereId($request->id)->first();

        return view('actuality',compact('actuality'));
    }


    public function actualities()
    {
        $actualities = Actuality::all();
        return view('actualities',compact('actualities'));
    }

    public function about()
    {
        $text = Text::whereName('about')->first();
        $about = $text->content;
        return view('about',compact('about'));

    }
    public function contact()
    {
        return view('contact');
    }

    public function contactPost(Request $request)
    {
        Mail::to('generalsecretary@anglican-bj.org')
            ->queue(new Contact([
                'name' => $request->name,
                'email' => $request->email,
                'message' => $request->message,
                'objet' => $request->objet,
            ]));
        //return response()->json(['success' => 'success']);
        $request->session()->flash('success', 'Informations envoyées avec succès!');

        return back();
    }

    public function prayPost(Request $request)
    {
        Mail::to('generalsecretary@anglican-bj.org')
            ->queue(new Contact([
                'name' => $request->name,
                'email' => $request->email,
                'message' => $request->message,
                'objet' =>'Demande de prière',
            ]));
        $request->session()->flash('success', 'Informations envoyées avec succès!');
        return back();
    }

    public function pray()
    {
        return view('pray');
    }
    public function prayStore()
    {
        return view('pray');
    }

    public function worships()
    {
        $worships = Worship::all();
        return view('worships',compact('worships'));
    }

    public function galleries()
    {
        return view('galleries');
    }
    public function lang($locale){
        App::setLocale($locale);
        Session::put('locale',$locale);
        Carbon::setLocale($locale);
        return back();
    }

    public function kkiapay(){

        $transaction_id = $_GET['transaction_id'];

        //mode live
        $kkiapay = new Kkiapay(env('KKIAPAY_PUBLIC_KEY'),env('KKIAPAY_PRIVATE_KEY'),env('KKIAPAY_SECRET'),true);

        $verification = $kkiapay->verifyTransaction($transaction_id);
//dd($verification);
        if ($verification->status=='SUCCESS'){
            Payment::create([
                'performed_at'=>Carbon::parse($verification->performed_at),
                'type'=>$verification->type,
                'status'=>$verification->status,
                'amount'=>$verification->amount,
                'fees'=>$verification->fees,
                'source'=>$verification->source,
                'transaction_id'=>$transaction_id,
                'source_common_name'=>$verification->source_common_name,
                'motif'=>'don',
            ]);
            Session::put('don', 'Informations envoyées avec succès!');

            return redirect(route('index'))->with(['error'=>false,'message'=>'Votre inscription a été effectué avec succès']);
        }else{
            return redirect(route('index'))->with(['error'=>true,'message'=>'Votre inscription a échoué, vueillez réessayer.']);
        }
    }
}


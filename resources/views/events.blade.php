@extends('template')
@section('nav')
    <span>{{__('sentences.menu_event')}}</span>
@endsection
@section('content')
    <section class="ftco-section ftco-section-2">
        <div class="container">
            <div class="row">
                @if(collect($events)->isNotEmpty())
                    @foreach($events as $event)
                        <div class="col-md-6 ftco-animate">
                            <div class="event-entry d-flex ">
                                <div class="text">
                                    <h3 class="mb-2"><a href="events.html">{{$event->titre}}</a></h3>
                                    <p class="mb-4">  <span>{{ date('d/m/Y',strtotime($event->date_start))}}</span> <span> {{ $event->venue}}</span></p>
                                    <a href="" class="img mb-4" style="background-image: url({{asset('storage/'.$event->image)}});"></a>
                                    <p>{{substr($event->description, 0, 200)}} @if(strlen($event->description) >=50)... @endif</p>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endif

                <div class="col-md-6">
                    <div class="event-entry d-flex ftco-animate">
                        <div class="meta mr-4">
                            <p>
                                <span>07</span>
                                <span>Aug 2018</span>
                            </p>
                        </div>
                        <div class="text">
                            <h3 class="mb-2"><a href="events.html">Saturday's Bible Reading</a></h3>
                            <p class="mb-4"><span>9:00am at 456 NC USA</span></p>
                            <a href="events.html" class="img mb-4" style="background-image: url(images/event-1.jpg);"></a>
                            <p>A small river named Duden flows by their place and supplies it with the necessary regelialia.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="event-entry d-flex ftco-animate">
                        <div class="meta mr-4">
                            <p>
                                <span>07</span>
                                <span>Aug 2018</span>
                            </p>
                        </div>
                        <div class="text">
                            <h3 class="mb-2"><a href="events.html">Saturday's Bible Reading</a></h3>
                            <p class="mb-4"><span>9:00am at 456 NC USA</span></p>
                            <a href="events.html" class="img mb-4" style="background-image: url(images/event-2.jpg);"></a>
                            <p>A small river named Duden flows by their place and supplies it with the necessary regelialia.</p>
                        </div>
                    </div>
                </div>
            </div>
            {{--<div class="row mt-5">--}}
                {{--<div class="col text-center">--}}
                    {{--<div class="block-27">--}}
                        {{--<ul>--}}
                            {{--<li><a href="#">&lt;</a></li>--}}
                            {{--<li class="active"><span>1</span></li>--}}
                            {{--<li><a href="#">2</a></li>--}}
                            {{--<li><a href="#">3</a></li>--}}
                            {{--<li><a href="#">4</a></li>--}}
                            {{--<li><a href="#">5</a></li>--}}
                            {{--<li><a href="#">&gt;</a></li>--}}
                        {{--</ul>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}

        </div>
    </section>
@endsection
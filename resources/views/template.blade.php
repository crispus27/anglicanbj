
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Wisdom - Free Bootstrap 4 Template by Colorlib</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link href="https://fonts.googleapis.com/css?family=Work+Sans:100,200,300,400,500,600,700,800" rel="stylesheet">

    <link rel="stylesheet" href="{{asset('css/open-iconic-bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/animate.css')}}">
    <link rel="stylesheet" href="{{asset('css/animate.css')}}">

    <link rel="stylesheet" href="{{asset('css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/owl.theme.default.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/magnific-popup.css')}}">

    <link rel="stylesheet" href="{{asset('css/aos.css')}}">

    <link rel="stylesheet" href="{{asset('css/ionicons.min.css')}}">

    <link rel="stylesheet" href="{{asset('css/bootstrap-datepicker.css')}}">
    <link rel="stylesheet" href="{{asset('css/jquery.timepicker.css')}}">


    <link rel="stylesheet" href="{{asset('css/flaticon.css')}}">
    <link rel="stylesheet" href="{{asset('css/icomoon.css')}}">
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
    <link rel="stylesheet" href="{{asset('plugin/sweetalert2/sweetalert2.css')}}">
    {{--<script src="https://cdn.kkiapay.me/k.js"> </script>--}}
    @yield('css')
</head>
<body>
@include('partial.nav')
<!-- END nav -->

<section id="home" class="video-hero js-fullheight" style="height: 700px; background-image: url(images/bg_1.jpg);  background-size:cover; background-position: center center;background-attachment:fixed;" data-section="home">
    <div class="overlay js-fullheight"></div>
    {{--<a class="player" data-property="{videoURL:'https://www.youtube.com/watch?v=5m--ptwd_iI',containment:'#home', showControls:false, autoPlay:true, loop:true, mute:false, startAt:0, opacity:1, quality:'default'}"></a>--}}
    <div class="container">
        <div class="row no-gutters slider-text js-fullheight align-items-center justify-content-center" data-scrollax-parent="true">
            <div class="col-md-10 ftco-animate text-center" data-scrollax=" properties: { translateY: '70%' }">
                <p class="breadcrumbs mb-2" data-scrollax="properties: { translateY: '30%', opacity: 1.6 }"><span class="mr-2"><a href="{{route('index')}}"> {{__('sentences.menu_home')}}</a></span>  @yield('nav') </p>

                <h1 class="mb-3 mt-0 bread" data-scrollax="properties: { translateY: '30%', opacity: 1.6 }">
                    {{__('sentences.home_text')}}
                  </h1>
            </div>
        </div>
    </div>
</section>


{{--<section class="ftco-bible-study">--}}
    {{--<div class="container-wrap">--}}
        {{--<div class="col-md-12 wrap">--}}
            {{--<div class="container">--}}
                {{--<div class="row">--}}
                    {{--<div class="col-md-12 d-md-flex">--}}
                        {{--<div class="one-forth ftco-animate">--}}
                            {{--<h3>Bible Study</h3>--}}
                            {{--<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia.</p>--}}
                        {{--</div>--}}

                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
{{--</section>--}}
@yield('content')

<div class="container">
    <div class="row">
        {{--<div class="col-md-6 ">--}}
            {{--<div class="card text-center">--}}
                {{--<div class="card-header">--}}
                    {{--Don--}}
                {{--</div>--}}
                {{--<div class="card-body">--}}
                    {{--<h5 class="card-title">L’Église assure ses missions grâce à la générosité des fidèles </h5>--}}
                    {{--<p class="card-text">Et n'oubliez pas la bienfaisance et la libéralité, c'est dans des tels sacrifices que Dieu prend plaisir. (Hébreux 13:16)</p>--}}
                    {{--<a href="#" class="btn btn-primary">Faites un don</a>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}

    </div>
</div>

{{--<button type="button" class="btn btn-danger btn-lg btn-block">Faire un don</button>--}}
@include('partial.footer')

<!-- loader -->
<div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>


<script src="{{asset('js/jquery.min.js')}}"></script>
<script src="{{asset('js/jquery-migrate-3.0.1.min.js')}}"></script>
<script src="{{asset('js/popper.min.js')}}"></script>
<script src="{{asset('js/bootstrap.min.js')}}"></script>
<script src="{{asset('js/jquery.easing.1.3.js')}}"></script>
<script src="{{asset('js/jquery.waypoints.min.js')}}"></script>
<script src="{{asset('js/jquery.stellar.min.js')}}"></script>
<script src="{{asset('js/owl.carousel.min.js')}}"></script>
<script src="{{asset('js/jquery.magnific-popup.min.js')}}"></script>
<script src="{{asset('js/aos.js')}}"></script>
<script src="{{asset('js/jquery.animateNumber.min.js')}}"></script>
<script src="{{asset('js/bootstrap-datepicker.js')}}"></script>
<script src="{{asset('js/jquery.timepicker.min.js')}}"></script>
<script src="{{asset('js/jquery.mb.YTPlayer.min.js')}}"></script>
<script src="{{asset('js/scrollax.min.js')}}"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
<script src="{{asset('js/google-map.js')}}"></script>
<script src="{{asset('js/main.js')}}"></script>
<script src="https://cdn.kkiapay.me/k.js"> </script>
<script src="{{asset('plugin/sweetalert2/sweetalert2.js')}}"></script>

<script>
    // $(document).on('click','montant',function (e) {
    // });
    function montant() {
        console.log('debut');
         Swal.fire({
            title: 'Montant du don en FCFA',
            input: 'text',
            inputPlaceholder:'{{__('sentences.enter_amount')}}'
        }).then((result) => {
             if (result.value) {
                 console.log(result.value);
                 var amount = result.value;
                 kkiapay(amount);
             }
         });
    }
</script>
<script>
    function kkiapay(amount) {
        openKkiapayWidget({
            amount: amount,
            position:"right",
            callback:"{{route('kkiapay')}}",
            //data=""
            theme:"blue",
            sandbox:"true",
            key:"80d6ced084c011ea9644e7b54342544d"})
    }
</script>


    @if(session()->get('don'))
        <script>
            const Toast = Swal.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 3000,
                timerProgressBar: true,
                onOpen: (toast) => {
                    toast.addEventListener('mouseenter', Swal.stopTimer)
                    toast.addEventListener('mouseleave', Swal.resumeTimer)
                }
            });

            Toast.fire({
                icon: 'success',
                title: 'Merci pour votre don'
            })
        </script>
        {{\Illuminate\Support\Facades\Session::forget('don')}}
    @endif


@yield('js')
</body>
</html>
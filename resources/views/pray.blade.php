@extends('template')
@section('css')
    <link rel="stylesheet" href="{{asset('css/pray.css')}}">
@endsection
@section('nav')
    <span>Prière</span>
@endsection

@section('content')
    <div class="page-wrapper bg-dark p-t-100 p-b-50">
        <div class="wrapper wrapper--w900">
            <div class="card card-6">
                <div class="card-heading">
                    <h2 class="title">Déposer une intention de prière</h2>
                </div>
                <div class="card-body">
                    <form method="POST" action="{{route('prayPost')}}">
                        @csrf
                        <div class="form-row">
                            <div class="name">Nom Prénoms</div>
                            <div class="value">
                                <input class="input--style-6" type="text" name="name"">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="name">Addresse Email</div>
                            <div class="value">
                                <div class="input-group">
                                    <input class="input--style-6" type="email" name="email" placeholder="example@email.com">
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="name">Message</div>
                            <div class="value">
                                <div class="input-group">
                                    <textarea class="textarea--style-6" name="message" placeholder="Entrez ici votre demande"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <button class="btn-pray btn-blue-anglican " type="submit">Envoyez</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>


@endsection
@section('js')
    @if(session()->get('success'))
        <script>
            const Toast = Swal.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 3000,
                timerProgressBar: true,
                onOpen: (toast) => {
                    toast.addEventListener('mouseenter', Swal.stopTimer)
                    toast.addEventListener('mouseleave', Swal.resumeTimer)
                }
            });

            Toast.fire({
                icon: 'success',
                title: 'Demande de prière envoyé avec succès'
            })
        </script>
    @endif
@endsection
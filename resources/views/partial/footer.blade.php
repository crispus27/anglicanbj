<footer class="ftco-footer ftco-bg-dark ftco-section">
    <div class="container">
        <div class="row mb-5">
            <div class="col-md">
                <div class="ftco-footer-widget mb-4">
                    <h2 class="logo"><i class="flaticon-cross"></i><a href="index.html">Eglise anglicane du Bénin</a></h2>
                    {{--<p>CHURCH OF BENIN</p>--}}
                </div>
            </div>
            <div class="col-md">
                <div class="ftco-footer-widget mb-4 ml-md-5">
                    <h2 class="ftco-heading-2">Liens rapides</h2>
                    <ul class="list-unstyled">
                        {{--<li><a href="{{route('about')}}" class="py-2 d-block">A propos{{__('sentences.menu_about')}}</a></li>--}}
                        <li ><a href="{{route('events')}}" class="py-2 d-block">{{__('sentences.menu_event')}}</a></li>
                        <li ><a href="{{route('actualities')}}" class="py-2 d-block">{{__('sentences.menu_actualities')}}</a></li>

                        <li ><a href="{{route('contact')}}" class="py-2 d-block">{{__('sentences.menu_contact')}}</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md">
                <div class="ftco-footer-widget mb-4">
                    <div class="block-23 mb-3">
                        <ul>
                            <li><span class="icon icon-map-marker"></span><span class="text">08 BP 0047 Tri-postal, Cotonou-République du Bénin</span></li>
                            <li><a href="#"><span class="icon icon-phone"></span><span class="text">+ 229 97538904, <br> + 229 95764545</span></a></li>
                            <li><a href="#"><span class="icon icon-envelope"></span><span class="text">generalsecretary@anglican-bj.org</span></a></li>
                            {{--<li><span class="icon icon-clock-o"></span><span class="text">Saturday &mdash; Sunday 8:00am - 5:00pm</span></li>--}}
                        </ul>
                    </div>
                    <ul class="ftco-footer-social list-unstyled float-md-left float-lft">
                        <li class="ftco-animate"><a href="#"><span class="icon-twitter"></span></a></li>
                        <li class="ftco-animate"><a href="#"><span class="icon-facebook"></span></a></li>
                        <li class="ftco-animate"><a href="#"><span class="icon-instagram"></span></a></li>
                    </ul>
                </div>
            </div>

        </div>
        <div class="row">
            <div class="col-md-12 text-center">

                <p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                    Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | EGLISE ANGLICANE DU BÉNIN
                    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
            </div>
        </div>
    </div>
</footer>
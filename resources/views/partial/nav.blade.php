
<nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
    <div class="container">
        <a class="navbar-brand" href="{{route('index')}}">
            <img width="90" height="90" src="{{asset('images/Logo.png')}}" alt="anglican-bj">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="oi oi-menu"></span> Menu
        </button>


        {{--<a href="">--}}
        {{--<img class="logotype" src="{{asset('images/Logo2.png')}}" alt="stephane boss'art">--}}
        {{--</a>--}}


        <div class="collapse navbar-collapse" id="ftco-nav">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item {{Route::is('index') ? 'active' : '' }}"><a href="{{route('index')}}" class="nav-link">{{__('sentences.menu_home')}}</a></li>
                {{--<li class="nav-item {{Route::is('about') ? 'active' : '' }}" ><a href="{{route('about')}}" class="nav-link">Bienvenue</a></li>--}}
                <li class="nav-item {{Route::is('worships') ? 'active' : '' }}"><a href="{{route('worships')}}" class="nav-link">{{__('sentences.menu_worship')}}</a></li>
                {{--<li class="nav-item"><a href="" class="nav-link">Prière et apprentissage</a></li>--}}
                <li class="nav-item {{Route::is('events') ? 'active' : '' }}"><a href="{{route('events')}}" class="nav-link">{{__('sentences.menu_event')}}</a></li>
                <li class="nav-item {{Route::is('actualities') ? 'active' : '' }}"><a href="{{route('actualities')}}" class="nav-link">{{__('sentences.menu_actualities')}}</a></li>

                <li class="nav-item {{Route::is('contact') ? 'active' : '' }}"><a href="{{route('contact')}}" class="nav-link">{{__('sentences.menu_contact')}}</a></li>
                <li class="nav-item">
                    <button type="button" onclick="montant()" class="montant btn btn-blue-anglican d-lg-block m-l-15 mt-2" style="padding: 5px 20px; color: #fff;" >
                            <i class="fa fa-plus-circle">
                            </i> {{__('sentences.menu_don')}}
                    </button>
                </li>
                <li class="nav-item "><a href="{{ url('locale/fr') }}" style="color: {{(app()->getLocale()=='fr')?'#02a551':''}}" class="nav-link"><i class="fa fa-language"></i>FR</a></li>
                <li class="nav-item "><a href="{{ url('locale/en') }}" style="color: {{(app()->getLocale()=='en')?'#02a551':''}}" class="nav-link"><i class="fa fa-language"></i>EN</a></li>


                {{--<li class="nav-item">--}}
                    {{--<button type="button" class="kkiapay-button btn btn-info d-none d-lg-block m-l-15 mt-2 p-link-addproduit" style="padding: 0px 10px;" ><a href="" style="color: #fff;" class="btn" >--}}
                            {{--<i class="fa fa-plus-circle">--}}
                            {{--</i> Faire un don--}}
                        {{--</a>--}}
                    {{--</button>--}}
                {{--</li>--}}

                {{--<li class="nav-item ">--}}
                        {{--<div class="dropdown nav-link">--}}
                                {{--Plus--}}
                            {{--<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">--}}
                                {{--<a class="dropdown-item" href="#">Contact</a>--}}
                                {{--<a class="dropdown-item" href="#">Actualités</a>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                {{--</li>--}}



            </ul>
        </div>
    </div>
</nav>


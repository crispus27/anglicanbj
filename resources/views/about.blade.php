@extends('template')
@section('nav')
    <span>A propos</span>
@endsection

@section('content')


    <section class="ftco-section-2">
        <div class="container-fluid">
            <div class="section-2-blocks-wrapper d-flex row no-gutters">
                <div class="img col-md-6 ftco-animate" style="background-image: url('images/about.jpg'); width: 900px;height :900px ">
                </div>
                <div class="text col-md-6 ftco-animate">
                    <div class="text-inner align-self-start">

                        <h3>History</h3>
                        <p>L'Église du Nigéria a connu des années mouvementées de son histoire en tant que province autonome dans la communion anglicane à travers le monde.
                        </p>

                        <p>L'histoire remonte à 1906 lors d'une conférence des évêques en communion avec l'Église anglicane tenue à Lagos. Le Rt. Le révérend E.H. Elwin, alors évêque de Sierra Leone, a présidé la réunion. Le Rt. Le révérend Herbert Tugwell (évêque de l'Afrique équatoriale occidentale) était là avec quatre de ses évêques adjoints: Charles Phillips, Isaac Oluwole, James Johnson et N. Temple Hamlyn. C'est là que le besoin d'une Province de l'Afrique de l'Ouest a été souligné pour la première fois.

                            Une deuxième conférence à cet effet a de nouveau eu lieu à Lagos en 1935. Mais c'est la conférence du 30 octobre - 3 novembre 1944, également à Lagos, qui a fait un pas en avant clair sur cette question, conduisant d'abord à l'inauguration de l'église de la Province d'Afrique de l'Ouest à Freetown, Sierra Leone. Cela a été fait le 17 avril 1951 par l'archevêque de Canterbury de l'époque, le Très Révérend. Geoffrey Fisher. L'évêque de Lagos, le Rt. Revd. L.G. Vining a été élu premier archevêque de la nouvelle Province compromis ces cinq diocèses: Sierra Leone (1852), Accra (1909), Lagos (1919), Sur le Niger (1920) et Gambie (1935).
                        </p>

                    </div>
                </div>


                <div class="text col-md-12 ftco-animate" style="margin-top: -15%">
                    <div class="text-inner align-self-start">

                        <p>Entre 1951 et 1977, les deux diocèses du Nigeria (Lagos et sur le Niger) ont donné naissance à quatorze nouveaux: Delta du Niger, Ibadan et Ondo / Bénin (tous créés en 1952); Nigéria du Nord (1954); Owerri (1959); Bénin (1962); Ekiti (1966); Enugu (Ilesha (1974); Egba / Egbado et Ijebu (1976); Asaba (1977). </p>
                        <p>
                            Ces seize diocèses du Nigeria ont rapidement commencé à ressentir un besoin croissant de contextualisation de leur témoignage chrétien. L'occasion s'est finalement présentée lors d'un synode épiscopal à Ado-Ekiti le 31 janvier 1974. Là, ils ont décidé de lancer le processus de devenir une province autonome au sein de la Communion anglicane. Cela a été suivi de près par le Comité permanent de l'Église de la Province de l'Afrique de l'Ouest, qui lui a donné sa bénédiction et l'a renvoyé au Synode, qui s'est tenu sur le campus de l'Université de Lagos le 14 août 1975 et a adopté la résolution mettre en marche le mécanisme de l'actualisation de ce désir.

                            Connue alors sous le nom d'Association des diocèses anglicans du Nigéria (AADN), un comité de rédaction de la constitution a été mis en place sous la présidence de Sir Louis Mbanefo (de mémoire bénie). Le Conseil consultatif anglican réuni à Trinidad (23 mars - 2 avril 1976) a considéré que le projet était «en ordre» et l'a adopté comme «Résolution 34 sur la proposition de province du Nigéria».
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="ftco-section testimony-section bg-light">
        <div class="container">
            <div class="row justify-content-center mb-5 pb-5">
                <div class="col-md-7 text-center heading-section ftco-animate">
                    <h2 class="mb-4">Nos pasteurs</h2>
                </div>
            </div>
            <div class="row ftco-animate">
                <div class="col-md-12">
                    <div class="carousel-testimony owl-carousel ftco-owl">
                        <div class="item text-center">
                            <div class="testimony-wrap p-4 pb-5">
                                <div class="user-img mb-4" style="background-image: url(images/person_1.jpg)">
                    <span class="quote d-flex align-items-center justify-content-center">
                      <i class="icon-quote-left"></i>
                    </span>
                                </div>
                                <div class="text">
                                    <p class="mb-5">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
                                    <p class="name">Dennis Green</p>
                                    <span class="position">Member</span>
                                </div>
                            </div>
                        </div>
                        <div class="item text-center">
                            <div class="testimony-wrap p-4 pb-5">
                                <div class="user-img mb-4" style="background-image: url(images/person_2.jpg)">
                    <span class="quote d-flex align-items-center justify-content-center">
                      <i class="icon-quote-left"></i>
                    </span>
                                </div>
                                <div class="text">
                                    <p class="mb-5">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
                                    <p class="name">Dennis Green</p>
                                    <span class="position">Volunteer</span>
                                </div>
                            </div>
                        </div>
                        <div class="item text-center">
                            <div class="testimony-wrap p-4 pb-5">
                                <div class="user-img mb-4" style="background-image: url(images/person_3.jpg)">
                    <span class="quote d-flex align-items-center justify-content-center">
                      <i class="icon-quote-left"></i>
                    </span>
                                </div>
                                <div class="text">
                                    <p class="mb-5">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
                                    <p class="name">Dennis Green</p>
                                    <span class="position">Pastor</span>
                                </div>
                            </div>
                        </div>
                        <div class="item text-center">
                            <div class="testimony-wrap p-4 pb-5">
                                <div class="user-img mb-4" style="background-image: url(images/person_1.jpg)">
                    <span class="quote d-flex align-items-center justify-content-center">
                      <i class="icon-quote-left"></i>
                    </span>
                                </div>
                                <div class="text">
                                    <p class="mb-5">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
                                    <p class="name">Dennis Green</p>
                                    <span class="position">Guest</span>
                                </div>
                            </div>
                        </div>
                        <div class="item text-center">
                            <div class="testimony-wrap p-4 pb-5">
                                <div class="user-img mb-4" style="background-image: url(images/person_1.jpg)">
                    <span class="quote d-flex align-items-center justify-content-center">
                      <i class="icon-quote-left"></i>
                    </span>
                                </div>
                                <div class="text">
                                    <p class="mb-5">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
                                    <p class="name">Dennis Green</p>
                                    <span class="position">Pastor</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>




@endsection
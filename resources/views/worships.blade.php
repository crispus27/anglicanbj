@extends('template')
@section('css')
    {{--<link rel="stylesheet" href="{{asset('css/pray.css')}}">--}}
@endsection
@section('nav')
    <span>{{__('sentences.menu_worship')}}</span>
@endsection

@section('content')
 <div class="container m-2">
     <div class="row">
         @if(collect($worships)->isNotEmpty())
             @foreach($worships as $worship)
                 <div class="list-group col-md-6">
                     <a href="" class="list-group-item list-group-item-action ">
                         <div class="d-flex w-100 justify-content-between">
                             <h5 class="mb-1">{{$worship->name}}</h5>
                         </div>
                         <p class="mb-1">{{$worship->description}}
                         </p>
                     </a>
                 </div>
             @endforeach
         @endif



         <div class="list-group col-md-6">
             <a href="" class="list-group-item list-group-item-action ">
                 <div class="d-flex w-100 justify-content-between">
                     <h5 class="mb-1"> Traditional Services: Sundays at 9h00 and 11h00</h5>
                 </div>
                 <p class="mb-1">Vous avez quelque chose qui vous tient à cœur ? <br>
                     Vous souhaitez confier une demande ou un remerciement au Seigneur ?
                     <br>
                     Vous souhaitez prier pour un proche en souffrance ou une personne en grand danger ?
                 </p>
             </a>
         </div>
     </div>

 </div>

@endsection
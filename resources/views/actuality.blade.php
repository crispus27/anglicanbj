@extends('template')

@section('nav')
    <span>Actualités</span>
@endsection
@section('content')
    <section class="ftco-section">
        <div class="container">
            <section class="ftco-section ftco-degree-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8 ftco-animate">
                            <h2 class="mb-3">{{$actuality->titre}}</h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis, eius mollitia suscipit, quisquam doloremque distinctio perferendis et doloribus unde architecto optio laboriosam porro adipisci sapiente officiis nemo accusamus ad praesentium? Esse minima nisi et. Dolore perferendis, enim praesentium omnis, iste doloremque quia officia optio deserunt molestiae voluptates soluta architecto tempora.
                            </p>
                            <p>
                                <img src="{{asset('storage/'.$actuality->image)}}" alt="" class="img-fluid">
                            </p>
                        </div> <!-- .col-md-8 -->
                    </div>
                </div>
            </section> <!-- .section -->

        </div>
    </section>


@endsection
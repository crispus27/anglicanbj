@extends('template')

@section('nav')
    <span>{{__('sentences.menu_contact')}}</span>
@endsection

@section('content')

    <section class="ftco-section contact-section ftco-degree-bg">
        <div class="container bg-light">
            <div class="row d-flex mb-5 contact-info">
                <div class="col-md-12 mb-4">
                    <h2 class="h4">Information de contact</h2>
                </div>
                <div class="w-100"></div>
                <div class="col-md-4">
                    <p><span>Addresse:</span>08 BP 0047 Tri-postal, Cotonou-République du Bénin</p>
                </div>
                <div class="col-md-4">
                    <p><span>Tel:</span> <a href="tel://1234567920">+ 229 97538904, + 229 95764545</a></p>
                </div>
                <div class="col-md-4">
                    <p><span>Email:</span> <a href="mailto:info@yoursite.com">generalsecretary@anglican-bj.org</a></p>
                </div>

            </div>
            <div class="row block-9">
                <div class="col-md-6 pr-md-5">
                    <form action="{{route('contactPost')}}" method="post">
                        @csrf
                        <div class="form-group">
                            <input name="name" type="text" class="form-control" placeholder="Votre nom">
                        </div>
                        <div class="form-group">
                            <input name="email" type="text" class="form-control" placeholder="Votre Email">
                        </div>
                        <div class="form-group">
                            <input name="objet" type="text" class="form-control" placeholder="Objet">
                        </div>
                        <div class="form-group">
                            <textarea name="message" id="" cols="30" rows="7" class="form-control" placeholder="Message"></textarea>
                        </div>
                        <div class="form-group">
                            <input type="submit" value="Envoyez le Message" class="btn btn-blue-anglican py-3 px-5">
                        </div>
                    </form>

                </div>

                <div class="col-md-6" id="map"></div>
            </div>
        </div>
    </section>

@endsection
@section('js')
    @if(session()->get('success'))
        <script>
            const Toast = Swal.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 3000,
                timerProgressBar: true,
                onOpen: (toast) => {
                    toast.addEventListener('mouseenter', Swal.stopTimer)
                    toast.addEventListener('mouseleave', Swal.resumeTimer)
                }
            });

            Toast.fire({
                icon: 'success',
                title: 'Mail envoyé avec succès'
            })        </script>
    @endif
@endsection
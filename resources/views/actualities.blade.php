@extends('template')

@section('nav')
    <span>   {{__('sentences.menu_actualities')}}</span>
@endsection
@section('content')
    <section class="ftco-section">
        <div class="container">
            <div class="row">
                @if(collect($actualities)->isNotEmpty())
                    @foreach($actualities as $actuality)

                        <div class="col-md-4 ftco-animate">
                            <div class="blog-entry">
                                <a href="{{route('actuality',$actuality->id)}}" class="block-20" style="background-image: url({{asset('storage/'.$actuality->image)}});">
                                </a>
                                <div class="text p-4 d-block">
                                    <div class="meta mb-3">
                                        <div><a href="#">{{ date('d/m/Y',strtotime($actuality->created_at))}}</a></div>
                                        {{--<div><a href="#" class="meta-chat"><span class="icon-chat"></span> 3</a></div>--}}
                                    </div>
                                    <h3 class="heading"><a href="{{route('actuality',$actuality->id)}}">{{substr($actuality->description, 0, 200)}} @if(strlen($actuality->description) >=50)... @endif</a></h3>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endif
                <div class="col-md-4 ftco-animate">
                    <div class="blog-entry">
                        <a href="blog-single.html" class="block-20" style="background-image: url('images/image_1.jpg');">
                        </a>
                        <div class="text p-4 d-block">
                            <div class="meta mb-3">
                                <div><a href="#">July 12, 2018</a></div>
                                <div><a href="#">Admin</a></div>
                                <div><a href="#" class="meta-chat"><span class="icon-chat"></span> 3</a></div>
                            </div>
                            <h3 class="heading"><a href="#">Even the all-powerful Pointing has no control about the blind texts</a></h3>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 ftco-animate">
                    <div class="blog-entry" data-aos-delay="100">
                        <a href="blog-single.html" class="block-20" style="background-image: url('images/image_2.jpg');">
                        </a>
                        <div class="text p-4">
                            <div class="meta mb-3">
                                <div><a href="#">July 12, 2018</a></div>
                                <div><a href="#">Admin</a></div>
                                <div><a href="#" class="meta-chat"><span class="icon-chat"></span> 3</a></div>
                            </div>
                            <h3 class="heading"><a href="#">Even the all-powerful Pointing has no control about the blind texts</a></h3>
                        </div>
                    </div>
                </div>
            </div>
            {{--<div class="row mt-5">--}}
                {{--<div class="col text-center">--}}
                    {{--<div class="block-27">--}}
                        {{--<ul>--}}
                            {{--<li><a href="#">&lt;</a></li>--}}
                            {{--<li class="active"><span>1</span></li>--}}
                            {{--<li><a href="#">2</a></li>--}}
                            {{--<li><a href="#">3</a></li>--}}
                            {{--<li><a href="#">4</a></li>--}}
                            {{--<li><a href="#">5</a></li>--}}
                            {{--<li><a href="#">&gt;</a></li>--}}
                        {{--</ul>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        </div>
    </section>


@endsection
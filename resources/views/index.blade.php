@extends('template')

@section('content')


    <section class="ftco-section-2">
        <div class="container-fluid">
            <div class="section-2-blocks-wrapper d-flex row no-gutters">
                <div class="text col-md-6 ftco-animate">
                    <div class="text-inner align-self-start">

                        <h3>
                            {{__('sentences.about_titre')}}</h3>
                        <p> {{__('sentences.about_text')}}

                    </div>
                </div>

                <div class="img col-md-6 ftco-animate" style="background-image: url('images/abou2t.jpg');">
                    <iframe width= "640" height= "460" src="https://www.youtube.com/embed/RrWk1GsARhM?autoplay=1?rel=0" allow="autoplay; encrypted-media" frameborder= "0" allowfullscreen>
                    </iframe>


                    {{--<div class="row">--}}
                        {{--<div class="list-group">--}}
                            {{--<a href="{{route('pray')}}" class="list-group-item list-group-item-action ">--}}
                                {{--<div class="d-flex w-100 justify-content-between">--}}
                                    {{--<h5 class="mb-1">  Déposer une intention de prière</h5>--}}
                                {{--</div>--}}
                                {{--<p class="mb-1">Vous avez quelque chose qui vous tient à cœur ? <br>--}}
                                    {{--Vous souhaitez confier une demande ou un remerciement au Seigneur ?--}}
                                    {{--<br>--}}
                                    {{--Vous souhaitez prier pour un proche en souffrance ou une personne en grand danger ?</p>--}}
                            {{--</a>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                </div>
            </div>
        </div>
    </section>

    <section class="ftco-section">
        <div class="container">
            {{--<div class="row justify-content-center mb-5 pb-5">--}}
                {{--<div class="col-md-6 text-center heading-section ftco-animate">--}}
                    {{--<span class="subheading">Our Services</span>--}}
                    {{--<h2 class="mb-4">Giving light to someone</h2>--}}
                    {{--<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in</p>--}}
                {{--</div>--}}
            {{--</div>--}}
            <div class="row">
                <div class="col-md-6 col-lg-3 d-flex align-self-stretch ftco-animate">
                    <div class="media block-6 services d-block text-center">
                        <div class="d-flex justify-content-center"><div class="icon d-flex justify-content-center mb-3"><span class="align-self-center flaticon-planet-earth"></span></div></div>
                        <div class="media-body p-2 mt-3">
                            <h3 class="heading">
                               {{__('sentences.vision')}}</h3>
                            <p>{{__('sentences.vision_text')}}
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-3 d-flex align-self-stretch ftco-animate">
                    <div class="media block-6 services d-block text-center">
                        <div class="d-flex justify-content-center"><div class="icon d-flex justify-content-center mb-3"><span class="align-self-center flaticon-maternity"></span></div></div>
                        <div class="media-body p-2 mt-3">
                            <h3 class="heading">{{__('sentences.mission')}}</h3>
                            <p>
                                {{__('sentences.mission_text')}}  </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-3 d-flex align-self-stretch ftco-animate">
                    <div class="media block-6 services d-block text-center">
                        <div class="d-flex justify-content-center"><div class="icon d-flex justify-content-center mb-3"><span class="align-self-center flaticon-pray"></span></div></div>
                        <div class="media-body p-2 mt-3">
                            <h3 class="heading">{{__('sentences.integrite')}}</h3>
                            <p>{{__('sentences.integrite_text')}}
                            </p>
                        </div>
                    </div>
                </div>

                <div class="col-md-6 col-lg-3 d-flex align-self-stretch ftco-animate">
                    <div class="media block-6 services d-block text-center">
                        <div class="d-flex justify-content-center"><div class="icon d-flex justify-content-center mb-3"><span class="align-self-center flaticon-podcast"></span></div></div>
                        <div class="media-body p-2 mt-3">
                            <h3 class="heading"> {{__('sentences.trinite')}}</h3>
                            <p>
                                {{__('sentences.trinite_text')}}    </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="ftco-section ftco-section-2 bg-light">
        <div class="container">
            <div class="row justify-content-center mb-5 pb-5">
                <div class="col-md-7 text-center heading-section ftco-animate">
                    <h2>{{__('sentences.menu_event')}}</h2>
                </div>
            </div>
            <div class="row">
                @if(collect($events)->isNotEmpty())
                    @foreach($events as $event)
                        <div class="col-md-6 ftco-animate">
                            <div class="event-entry d-flex ">
                                <div class="text">
                                    <h3 class="mb-2"><a href="events.html">{{$event->titre}}</a></h3>
                                    <p class="mb-4">  <span>{{ date('d/m/Y',strtotime($event->date_start))}}</span> <span> {{ $event->venue}}</span></p>
                                    <a href="" class="img mb-4" style="background-image: url({{asset('storage/'.$event->image)}});"></a>
                                    <p>{{substr($event->description, 0, 200)}} @if(strlen($event->description) >=50)... @endif</p>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endif
                <div class="col-md-6 ftco-animate">
                    <div class="event-entry d-flex ">
                        <div class="text">
                            <h3 class="mb-2"><a href="events.html">Saturday's Bible Reading</a></h3>
                            <p class="mb-4">  <span>07 Aug 2018</span> <span>9:00am at 456 NC USA</span></p>
                            <a href="events.html" class="img mb-4" style="background-image: url(images/event-1.jpg);"></a>
                            <p>A small river named Duden flows by their place and supplies it with the necessary regelialia.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 ftco-animate">
                    <div class="event-entry d-flex ">
                        <div class="text">
                            <h3 class="mb-2"><a href="events.html">Saturday's Bible Reading</a></h3>
                            <p class="mb-4">  <span>07 Aug 2018</span> <span>9:00am at 456 NC USA</span></p>
                            <a href="events.html" class="img mb-4" style="background-image: url(images/event-2.jpg);"></a>
                            <p>A small river named Duden flows by their place and supplies it with the necessary regelialia.</p>
                        </div>
                    </div>
                </div>

            </div>
            <div class="row mt-5">
                <div class="col text-center">
                    <div class="block-27">
                        <a href="{{route('events')}}"> <button class="btn btn-primary btn--blue-2" type="submit">{{__('sentences.see_more')}}</button></a>

                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="ftco-section">
        <div class="container">
            <div class="row justify-content-center mb-5 pb-5">
                <div class="col-md-7 text-center heading-section ftco-animate">
                    <h2>{{__('sentences.menu_actualities')}}</h2>
                </div>
            </div>
            <div class="row">
                @if(collect($actualities)->isNotEmpty())
                    @foreach($actualities as $actuality)

                        <div class="col-md-4 ftco-animate">
                            <div class="blog-entry">
                                <a href="{{route('actuality',$actuality->id)}}" class="block-20" style="background-image: url({{asset('storage/'.$actuality->image)}});">
                                </a>
                                <div class="text p-4 d-block">
                                    <div class="meta mb-3">
                                        <div><a href="#">{{ date('d/m/Y',strtotime($actuality->created_at))}}</a></div>
                                        {{--<div><a href="#" class="meta-chat"><span class="icon-chat"></span> 3</a></div>--}}
                                    </div>
                                    <h3 class="heading"><a href="{{route('actuality',$actuality->id)}}">{{substr($actuality->description, 0, 200)}} @if(strlen($actuality->description) >=50)... @endif</a></h3>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endif

                <div class="col-md-4 ftco-animate">
                    <div class="blog-entry">
                        <a href="blog-single.html" class="block-20" style="background-image: url('images/image_1.jpg');">
                        </a>
                        <div class="text p-4 d-block">
                            <div class="meta mb-3">
                                <div><a href="#">July 12, 2018</a></div>
                                <div><a href="#">Admin</a></div>
                                <div><a href="#" class="meta-chat"><span class="icon-chat"></span> 3</a></div>
                            </div>
                            <h3 class="heading"><a href="#">Even the all-powerful Pointing has no control about the blind texts</a></h3>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 ftco-animate">
                    <div class="blog-entry" data-aos-delay="100">
                        <a href="blog-single.html" class="block-20" style="background-image: url('images/image_2.jpg');">
                        </a>
                        <div class="text p-4">
                            <div class="meta mb-3">
                                <div><a href="#">July 12, 2018</a></div>
                                <div><a href="#">Admin</a></div>
                                <div><a href="#" class="meta-chat"><span class="icon-chat"></span> 3</a></div>
                            </div>
                            <h3 class="heading"><a href="#">Even the all-powerful Pointing has no control about the blind texts</a></h3>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mt-5">
                <div class="col text-center">
                    <div class="block-27">
                        <a href="{{route('actualities')}}"> <button class="btn btn-primary btn--blue-2" type="submit">{{__('sentences.see_more')}}</button></a>

                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="col-md-10 offset-md-1">
        <div class="card text-center">
            <div class="card-header">
             {{__('sentences.pray_titre')}}
            </div>
            <div class="card-body">
                <h5 class="card-title"></h5>
                <p class="card-text"> <br>

                    {{__('sentences.pray_text1')}}
                    <br>
                    {{__('sentences.pray_text2')}}
                    <br>
                    {{__('sentences.pray_text3')}}
                   </p>
                {{--<p class="card-text">Vous souhaitez confier une demande ou un remerciement au Seigneur ?</p>--}}
                {{--<p class="card-text">Vous souhaitez prier pour un proche en souffrance ou une personne en grand danger ?</p>--}}
                <a href="{{route('pray')}}" class="btn btn-blue-anglican">{{__('sentences.pray_titre')}}</a>
            </div>
        </div>
    </div>
@endsection
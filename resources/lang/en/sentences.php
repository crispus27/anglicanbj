<?php

return [
    'menu_home'=>'Home',
    'menu_worship'=>'Cultes et missions',
    'menu_event'=>'Event',
    'menu_actualities'=>'Actualities',
    'menu_contact'=>'Contact',
    'menu_don'=>'Faire un don',
    'home_text'=>'Church anglicane of Bénin',
    'about_titre'=>'Bienvenue à l\'église du Bénin',
    'about_text'=>'Le christianisme est entré au Bénin au 15ème siècle grâce aux efforts des moines augustins et capucins du Portugal. Cependant, ce n\'est qu\'en 1842 qu\'Henry Townsend de la Church Missionary Society (CMS) a semé correctement
     la graine de l\'anglicanisme lorsqu\'il a atterri à Badagry de Freetown, en Sierra Leone.',
    'vision'=>'Notre vision',
    'vision_text'=>'Une Bible basée, spirituellement dynamique, unie, disciplinée, autosuffisante, engagée dans l\'évangélisation pragmatique, le bien-être social et une Église qui incarne l\'amour authentique du Christ',
    'mission'=>'Notre mission',
    'mission_text'=>'Le programme d\'action comprend, entre autres, des traductions supplémentaires de la liturgie, la mise en place d\'une équipe de collecte de fonds laïque, la mise en place d\'un soutien juridique pour garantir la liberté de religion et de culte, et la création d\'industries artisanales.',
    'integrite'=>'Intégrité crédible',
    'integrite_text'=>'Identification des anglicans disciplinés fondés sur la Bible et nomination de ceux-ci au Global Anglican Relations Office / Ministry. Restructuration de CANA afin qu\'elle soit plus pertinente pour le mandat de la mission',
    'trinite'=>'Sainte Trinité',
    'trinite_text'=>'Nous croyons en Dieu, le Père Tout-Puissant, Créateur du ciel et de la terre, et en Jésus-Christ, son Fils unique, notre Seigneur, qui a été conçu par le Saint-Esprit, né de la Vierge Marie, souffert sous Ponce Pilate, a été crucifié',
    'event'=>'',
    'see_more'=>'',
    'actualities_recent'=>'',
    'pray_titre'=>'',
    'pray_text'=>'',
    ''=>'Liens_rapide',
    'contact_information'=>'',
    'sent_message'=>'',
    'quick_liens'=>'',
    'churh_of_benin'=>'',
    'adress'=>'',
    'your_name'=>'',
    'your_email'=>'',
    'your_message'=>'',
    'sent_message_contact'=>'',
];

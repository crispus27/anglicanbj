<?php

return [
    'menu_home'=>'Acceuil',
    'menu_worship'=>'Cultes et missions',
    'menu_event'=>'Evènements',
    'menu_actualities'=>'Actualités',
    'menu_contact'=>'Contacts',
    'menu_don'=>'Faire un don',
    'home_text'=>'Eglise anglicane du Bénin',
    'enter_amount'=>'Entrez le montant de votre don',
    'about_titre'=>'Bienvenue à l\'église du Bénin',
    'about_text'=>'Le christianisme est entré au Bénin au 15ème siècle grâce aux efforts des moines augustins et capucins du Portugal. Cependant, ce n\'est qu\'en 1842 qu\'Henry Townsend de la Church Missionary Society (CMS) a semé correctement
     la graine de l\'anglicanisme lorsqu\'il a atterri à Badagry de Freetown, en Sierra Leone.',
    'vision'=>'Notre vision',
    'vision_text'=>'Une Bible basée, spirituellement dynamique, unie, disciplinée, autosuffisante, engagée dans l\'évangélisation pragmatique, le bien-être social et une Église qui incarne l\'amour authentique du Christ',
    'mission'=>'Notre mission',
    'mission_text'=>'Le programme d\'action comprend, entre autres, des traductions supplémentaires de la liturgie, la mise en place d\'une équipe de collecte de fonds laïque, la mise en place d\'un soutien juridique pour garantir la liberté de religion et de culte, et la création d\'industries artisanales.',
    'integrite'=>'Intégrité crédible',
    'integrite_text'=>'Identification des anglicans disciplinés fondés sur la Bible et nomination de ceux-ci au Global Anglican Relations Office / Ministry. Restructuration de CANA afin qu\'elle soit plus pertinente pour le mandat de la mission',
    'trinite'=>'Sainte Trinité',
    'trinite_text'=>'Nous croyons en Dieu, le Père Tout-Puissant, Créateur du ciel et de la terre, et en Jésus-Christ, son Fils unique, notre Seigneur, qui a été conçu par le Saint-Esprit, né de la Vierge Marie, souffert sous Ponce Pilate, a été crucifié',
    'event'=>'',
    'see_more'=>'Voir plus',
    'actualities_recent'=>'',
    'pray_titre'=>'Déposer une intention de prière',
    'pray_text1'=>'Vous avez quelque chose qui vous tient à cœur ?',
    'pray_text2'=>'Vous souhaitez confier une demande ou un remerciement au Seigneur ?',
    'pray_text3'=>' Vous souhaitez prier pour un proche en souffrance ou une personne en grand danger?',
    ''=>'Liens_rapide',
    'contact_information'=>'',
    'sent_message'=>'',
    'quick_liens'=>'',
    'churh_of_benin'=>'',
    'adress'=>'',
    'your_name'=>'',
    'your_email'=>'',
    'your_message'=>'',
    'sent_message_contact'=>'',
    ];

<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['prefix' => '/'], function () {
    Route::get('', 'Controller@index')->name('index');
    Route::get('events', 'Controller@events')->name('events');
    Route::get('actualities', 'Controller@actualities')->name('actualities');
    Route::get('actualit/{id}', 'Controller@actuality')->name('actuality');

    Route::get('galleries', 'Controller@galleries')->name('galleries');
    Route::get('about', 'Controller@about')->name('about');
    Route::get('contact', 'Controller@contact')->name('contact');
    Route::get('pray', 'Controller@pray')->name('pray');
    Route::get('worships', 'Controller@worships')->name('worships');
    Route::post('/contactPost', 'Controller@contactPost')->name('contactPost');
    Route::get('kkiapay',['as'=>'kkiapay','uses'=>'Controller@kkiapay']);
    Route::post('/prayPost', 'Controller@prayPost')->name('prayPost');
    Route::get('locale/{locale}',['as'=>'app.lang','uses'=>'Controller@lang']);

});

Route::group(['prefix' => '/dashboard'], function () {
    Route::get('/', 'adminController@index')->name('admin.index');

});

Route::group(['prefix' => 'admin'], function () {
    \TCG\Voyager\Facades\Voyager::routes();
});


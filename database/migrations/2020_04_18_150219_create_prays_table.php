<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePraysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prays', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->double('amount')->nullable(true);
            $table->string('Name')->nullable(true);
            $table->string('email')->nullable(true);
            $table->string('subjet');
            $table->longText('message');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prays');
    }
}
